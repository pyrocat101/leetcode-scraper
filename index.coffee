jsdom = require 'jsdom'
async = require 'async'
fs = require 'fs'

BASE_URL = "http://oj.leetcode.com"

scrapeQuestions = (url, cb) ->
  url = BASE_URL + url
  jsdom.env(
    url,
    ["http://code.jquery.com/jquery.js"],
    (err, window) ->
      if err then throw err
      $ = window.$
      question_title = $('.question-title h3').text()
      question_content = $('.question-content').html().trim()
      this_question =
        'url': url
        'title': question_title
        'content': question_content
      console.log "Scraped: #{url}"
      cb null, this_question
  )

jsdom.env(
  "http://oj.leetcode.com/problems/",
  ["http://code.jquery.com/jquery.js"],
  (err, window) ->
    if err then throw err
    $ = window.$
    question_urls = $("#problemList tbody a").map(-> $(this).attr('href')).get()
    async.map question_urls, scrapeQuestions, (err, results) ->
      json = JSON.stringify(results)
      fs.writeFile 'questions.json', json, (err) ->
        if err then throw err
        console.log 'Questions saved to question.json'
)