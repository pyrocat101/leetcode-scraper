fs = require 'fs'

fs.readFile 'questions.json', (err, data) ->
  if err then throw err
  questions = JSON.parse(data)
  html = "<h1>LeetCode Questions</h1>"
  for question in questions
    html += "<h2>#{question.title}</h2>"
    html += question.content
  fs.writeFile 'questions.html', html, (err) ->
    if err then throw err
    console.log 'Wrote questions to questions.html'